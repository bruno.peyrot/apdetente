﻿<
>
<
Qui a peint la Joconde ? 
Léonard de Vinci*
Pablo Picasso
Vincent Van Gogh
Claude Monet
>
<
En qu'elle année a-t-on fait le premier pas sur la Lune ?
1959
1973
1969*
>
<
Qu'elle est la date de la prise de la Bastille ?
14 juillet 1789*
14 août 1826
>
<
Combien de pays reconnus existe-t-il dans le monde ?
52
136
197*
>
<
Combien d"étoiles il y a-t-il sur la drapeau américain ?
50*
55
60
>
<
Qu'elle est la plus haute montagne du monde ?
Everest*
K2
Mont Blanc
>
<
Quel réseau social est le plus utilisé au monde ?
Youtube*
Instagram
TikTok
Facebook
>
<
Qu'elle est la circonférence de la Terre ? 
35 000m
40 000km*
15 000km
125 000km
>
<
Qu'elle est la voiture la plus vendu au monde ?
Renault Clio
Toyota Corolla*
Dacia Sandero
>
<
Combien sommes nous d'habitants en France ?
500 000 millions
68 millions*
7 millions
>
<
Combien d'humains sommes nous sur la Terre ?
1.5 milliard
6 milliard
8 milliard*
>
<
En qu'elle année a eu lieu les premiers jeux Olympiques ? 
1896*
1944
1960
>
<
Quel pays a gagné la coupe du monde de football en 2022 ?
France
Portugal
Argentine*
>
<
Quel sport est le plus pratique au monde ?
Rugby
Football*
Tennis
>
<
Quel pays a envoyer le premier homme dans l'espace ?
Etats-Unis
URSS*
Chine
>
<
Qui a gagner le championnat du monde de F1 en 2019 ?
Lando Norris
Max Verstappen
Charles Leclerc
Lewis Hamilton*
>
<
En qu'elle année a eu lieu le premier tour de France ?
1903*
1906
1909
>
<
Quelle discipline sportive enfreint le plus les règles antidopage au niveau mondial ?
L'athlétisme
Le cyclisme
Le culturisme*
>
<
La Fédération internationale de football, compte-t-elle plus d’adhérents que l’ONU ?
OUI*
NON
>
<
Quel célèbre personnage fête les 50 ans de sa création en 2024 ?
Mickey Mouse
Casper le gentil fantôme
Casimir*
>