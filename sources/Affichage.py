# Créé par pc, le 12/03/2024 en Python 3.7
import pygame
import time
import Player
from win32api import GetSystemMetrics

#ce dossier s'occupe de gérer le déplaxement et l'affichage des joueurs


def placement(nb_reponses,position_j,joueur,q_j,screen):# q_j c'est pour quelle joueur est demander
    x_ecran = GetSystemMetrics (0)
    y_ecran = GetSystemMetrics (1)

    for i in range(1,7):
            if q_j==i:# je rajoute ne fonction de quelle joueur a été choisie
                a_ajouter=i*40

    #si il n'y a que 2 réponses possibles
    if nb_reponses==2:
        #gère la placement des joueurs avec des proportions pour que ce soit adaptable a tous les ordinateurs
        if position_j==0:
            position= (x_ecran/100,y_ecran/1.75+a_ajouter)
        if position_j==1:
            position= (x_ecran/8+a_ajouter,y_ecran/1.40)
        if position_j==2:
            position= (x_ecran/1.70+a_ajouter,y_ecran/1.40)

    #si il y a 3 réponses
    if nb_reponses==3:

        if position_j==0:
            position= (x_ecran/100,y_ecran/1.75+a_ajouter)
        if position_j==1:
            position= (x_ecran/20+a_ajouter , y_ecran/1.4)
        if position_j==2:
            position= (x_ecran/2.7+a_ajouter , y_ecran/1.4)
        if position_j==3:
            position= (x_ecran/1.46+a_ajouter , y_ecran/1.4)

    #si il y a 4 réponses
    if nb_reponses==4:

        if position_j==0:
            position= (x_ecran/100,y_ecran/1.75+a_ajouter)
        if position_j==1:
            position= (x_ecran/5.9+a_ajouter , y_ecran/1.7)
        if position_j==2:
            position= (x_ecran/1.88+a_ajouter , y_ecran/1.7)
        if position_j==3:
            position= (x_ecran/5.9+a_ajouter , y_ecran/1.12)
        if position_j==4:
            position= (x_ecran/1.88+a_ajouter , y_ecran/1.12)

    #dessine le joueur avec sa bonne position
    joueur[q_j-1].draw(screen,position)



def rt_question(reponsesF,q):# le q est pour la question
    x_ecran = GetSystemMetrics (0)
    #police de notre écriture
    policeR = pygame.font.Font("Fonts/OctopusGame-RpWV3.ttf",round(x_ecran/65))
    reponse1pt1=""
    reponse1pt2=""
    g=0
    #gère le fait que la réponse ne depasse pas du cadre
    if len(reponsesF[q])>19:
        if len(reponsesF[q])>39:
            policeR = pygame.font.Font("Fonts/OctopusGame-RpWV3.ttf",round(x_ecran/90))
            g=8
        for i in range(19+g):
            reponse1pt1+=reponsesF[q][i]
        a=0
        while reponse1pt1[len(reponse1pt1)-1]!=" " and 19+a+g!=len(reponsesF[q]):
            reponse1pt1+=reponsesF[q][19+a+g]
            a+=1
        image_rep1pt1 = policeR.render(reponse1pt1,1,(0,0,0))
        for i in range(len(reponsesF[q])-19-a-g):
            reponse1pt2+=reponsesF[q][i+19+a+g]
        image_rep1pt2 = policeR.render(reponse1pt2,1,(0,0,0))
        return image_rep1pt1,image_rep1pt2
    else:
        image_rep1pt1 = policeR.render(reponsesF[q],1,(0,0,0))
        image_rep1pt2 = policeR.render("",1,(0,0,0))
        return image_rep1pt1,image_rep1pt2




def Question(question,reponses,screen,player1,player2,player3,player4,player5,player6,position_j1,position_j2,position_j3,position_j4,position_j5,position_j6,nb_joueur):
    joueur=[player1,player2,player3,player4,player5,player6]
    pygame.init()
    reponsesF = []
    #enlève les reponses vides
    for i in range(4):
        if reponses[i]!="":
            reponsesF.append(reponses[i])

    x_ecran = GetSystemMetrics (0)
    y_ecran = GetSystemMetrics (1)
    policeQ = pygame.font.Font("Fonts/OctopusGame-RpWV3.ttf",round(x_ecran/35))
    nb_mots=0
    questionpt1=""
    questionpt2=""

    #évite le fait que la question depasse du cadre
    if len(question)>38:
        for i in range(38):
            questionpt1+=question[i]
        a=0
        while questionpt1[len(questionpt1)-1]!=" ":
            questionpt1+=question[38+a]
            a+=1
        image_question = policeQ.render(questionpt1,1,(0,0,0))
        for i in range(len(question)-38-a):
            questionpt2+=question[i+38+a]
        image_question2 = policeQ.render(questionpt2,1,(0,0,0))
    else:
        image_question = policeQ.render(question,1,(0,0,0))

    #si il y a 2 réponses
    if len(reponsesF)==2:
        image_rep1pt1,image_rep1pt2=rt_question(reponsesF,0)
        image_rep2pt1,image_rep2pt2=rt_question(reponsesF,1)


        base = pygame.image.load("Images/2R_base.jpg")
        base = pygame.transform.scale( base , (x_ecran,y_ecran))

        #affiche le fon ainsi que la question
        screen.blit(base,(0,0))
        screen.blit(image_question,(0.06*x_ecran,y_ecran/6))
        #si la question est trop longue alors on affiche la fin de la question à la deuxième ligne
        if len(question)>45:
            screen.blit(image_question2,(0.06*x_ecran,y_ecran/4))

        #affiche les réponses sur une ou deux lignes
        screen.blit(image_rep1pt1,(0.135*x_ecran,y_ecran/1.7))
        screen.blit(image_rep1pt2,(0.135*x_ecran,y_ecran/1.5))

        screen.blit(image_rep2pt1,(0.60*x_ecran,y_ecran/1.7))
        screen.blit(image_rep2pt2,(0.60*x_ecran,y_ecran/1.5))

    if len(reponsesF)==3:
        #même système mais avec 3 réponses
        image_rep1pt1,image_rep1pt2=rt_question(reponsesF,0)
        image_rep2pt1,image_rep2pt2=rt_question(reponsesF,1)
        image_rep3pt1,image_rep3pt2=rt_question(reponsesF,2)

        base = pygame.image.load("Images/3R_base.jpg")
        base = pygame.transform.scale( base , (x_ecran,y_ecran))

        screen.blit(base,(0,0))
        screen.blit(image_question,(0.06*x_ecran,y_ecran/6))
        if len(question)>45:
            screen.blit(image_question2,(0.06*x_ecran,y_ecran/4))

        screen.blit(image_rep1pt1,(0.06*x_ecran,y_ecran/1.8))
        screen.blit(image_rep1pt2,(0.06*x_ecran,y_ecran/1.6))
        screen.blit(image_rep2pt1,(0.38*x_ecran,y_ecran/1.8))
        screen.blit(image_rep2pt2,(0.38*x_ecran,y_ecran/1.6))
        screen.blit(image_rep3pt1,(0.695*x_ecran,y_ecran/1.8))
        screen.blit(image_rep3pt2,(0.695*x_ecran,y_ecran/1.6))

    if len(reponsesF)==4:
        #même système mais avec 4 réponses
        image_rep1pt1,image_rep1pt2=rt_question(reponsesF,0)
        image_rep2pt1,image_rep2pt2=rt_question(reponsesF,1)
        image_rep3pt1,image_rep3pt2=rt_question(reponsesF,2)
        image_rep4pt1,image_rep4pt2=rt_question(reponsesF,3)

        base = pygame.image.load("Images/4R_base.jpg")
        base = pygame.transform.scale( base , (x_ecran,y_ecran))

        screen.blit(base,(0,0))

        screen.blit(image_question,(0.06*x_ecran,y_ecran/6))
        if len(question)>38:
            screen.blit(image_question2,(0.06*x_ecran,y_ecran/4))

        screen.blit(image_rep1pt1,(0.19*x_ecran,y_ecran/2.1))
        screen.blit(image_rep1pt2,(0.19*x_ecran,y_ecran/1.8))
        screen.blit(image_rep2pt1,(0.55*x_ecran,y_ecran/2.1))
        screen.blit(image_rep2pt2,(0.55*x_ecran,y_ecran/1.8))
        screen.blit(image_rep3pt1,(0.19*x_ecran,y_ecran/1.3))
        screen.blit(image_rep3pt2,(0.19*x_ecran,y_ecran/1.1))
        screen.blit(image_rep4pt1,(0.55*x_ecran,y_ecran/1.3))
        screen.blit(image_rep4pt2,(0.55*x_ecran,y_ecran/1.1))

    positions_t_j=[position_j1,position_j2,position_j3,position_j4,position_j5,position_j6]

    for i in range(nb_joueur):
        position_j=positions_t_j[i]
        placement(len(reponsesF),position_j,joueur,i+1,screen)


    pygame.display.flip()

def reponse(question,reponses,screen,player1,player2,player3,player4,player5,player6,position_j1,position_j2,position_j3,position_j4,position_j5,position_j6,nb_joueur,v_reponse,categories,ch):
    joueur=[player1,player2,player3,player4,player5,player6]
    dif_taille_v=10
    pygame.init()
    reponsesF = []
    #fait la liste des réponses sans celles qui sont vides
    for i in range(4):
        if reponses[i]!="":
            reponsesF.append(reponses[i])

    x_ecran = GetSystemMetrics (0)
    y_ecran = GetSystemMetrics (1)
    policeQ = pygame.font.Font("Fonts/OctopusGame-RpWV3.ttf",round(x_ecran/35))
    nb_mots=0
    questionpt1=""
    questionpt2=""

    #si la question est trop longue
    if len(question)>38:
        for i in range(38):
            questionpt1+=question[i]
        a=0
        while questionpt1[len(questionpt1)-1]!=" ":
            questionpt1+=question[38+a]
            a+=1
        image_question = policeQ.render(questionpt1,1,(0,0,0))
        for i in range(len(question)-38-a):
            questionpt2+=question[i+38+a]
        image_question2 = policeQ.render(questionpt2,1,(0,0,0))
    else:
        image_question = policeQ.render(question,1,(0,0,0))



    if len(reponsesF)==2:
        #si le nombre de réponses possibles est 2
        #gère la bonne réponse afin de zoomer sur la bonne réponse a la fin du timer

        if v_reponse == 1 and categories[ch]!="D":
            #change l'image de fond
            base = pygame.image.load("Images/2R_g.jpg")
            image_rep1pt1,image_rep1pt2=rt_question(reponsesF,0)
            #transforme l'image ed la taille del'écran
            base = pygame.transform.scale( base , (x_ecran,y_ecran))
            #affiche l'image de fond ainsi que la réponse
            screen.blit(base,(0,0))
            screen.blit(image_rep1pt1,(0.135*x_ecran,y_ecran/1.7))
            screen.blit(image_rep1pt2,(0.135*x_ecran,y_ecran/1.5))
        if v_reponse == 2 and categories[ch]!="D":
            base = pygame.image.load("Images/2R_d.jpg")
            image_rep2pt1,image_rep2pt2=rt_question(reponsesF,1)
            base = pygame.transform.scale( base , (x_ecran,y_ecran))
            screen.blit(base,(0,0))
            screen.blit(image_rep2pt1,(0.60*x_ecran,y_ecran/1.7))
            screen.blit(image_rep2pt2,(0.60*x_ecran,y_ecran/1.5))

    if len(reponsesF)==3:
        #même chose pour 3 réponses possibles
        if v_reponse == 1 and categories[ch]!="D":
            base = pygame.image.load("Images/3R_g.jpg")
            image_rep1pt1,image_rep1pt2=rt_question(reponsesF,0)
            base = pygame.transform.scale( base , (x_ecran,y_ecran))
            screen.blit(base,(0,0))
            screen.blit(image_rep1pt1,(0.06*x_ecran,y_ecran/1.8))
            screen.blit(image_rep1pt2,(0.06*x_ecran,y_ecran/1.6))

        if v_reponse == 2 and categories[ch]!="D":
            base = pygame.image.load("Images/3R_m.jpg")
            image_rep2pt1,image_rep2pt2=rt_question(reponsesF,1)
            base = pygame.transform.scale( base , (x_ecran,y_ecran))
            screen.blit(base,(0,0))
            screen.blit(image_rep2pt1,(0.38*x_ecran,y_ecran/1.8))
            screen.blit(image_rep2pt2,(0.38*x_ecran,y_ecran/1.6))

        if v_reponse == 3 and categories[ch]!="D":
            base = pygame.image.load("Images/3R_d.jpg")
            image_rep3pt1,image_rep3pt2=rt_question(reponsesF,2)
            base = pygame.transform.scale( base , (x_ecran,y_ecran))
            screen.blit(base,(0,0))
            screen.blit(image_rep3pt1,(0.695*x_ecran,y_ecran/1.8))
            screen.blit(image_rep3pt2,(0.695*x_ecran,y_ecran/1.6))



    if len(reponsesF)==4:
        #même chose pour réposes possibles
        if v_reponse == 1 and categories[ch]!="D":
            base = pygame.image.load("Images/4R_h_g.jpg")
            image_rep1pt1,image_rep1pt2=rt_question(reponsesF,0)
            base = pygame.transform.scale( base , (x_ecran,y_ecran))
            screen.blit(base,(0,0))
            screen.blit(image_rep1pt1,(0.19*x_ecran,y_ecran/2.1))
            screen.blit(image_rep1pt2,(0.19*x_ecran,y_ecran/1.8))
        if v_reponse == 2 and categories[ch]!="D":
            base = pygame.image.load("Images/4R_h_d.jpg")
            image_rep2pt1,image_rep2pt2=rt_question(reponsesF,1)
            base = pygame.transform.scale( base , (x_ecran,y_ecran))
            screen.blit(base,(0,0))
            screen.blit(image_rep2pt1,(0.55*x_ecran,y_ecran/2.1))
            screen.blit(image_rep2pt2,(0.55*x_ecran,y_ecran/1.8))
        if v_reponse == 3 and categories[ch]!="D":
            base = pygame.image.load("Images/4R_b_g.jpg")
            image_rep3pt1,image_rep3pt2=rt_question(reponsesF,2)
            base = pygame.transform.scale( base , (x_ecran,y_ecran))
            screen.blit(base,(0,0))
            screen.blit(image_rep3pt1,(0.19*x_ecran,y_ecran/1.3))
            screen.blit(image_rep3pt2,(0.19*x_ecran,y_ecran/1.1))
        if v_reponse == 4 and categories[ch]!="D":
            base = pygame.image.load("Images/4R_b_d.jpg")
            image_rep4pt1,image_rep4pt2=rt_question(reponsesF,3)
            base = pygame.transform.scale( base , (x_ecran,y_ecran))
            screen.blit(base,(0,0))
            screen.blit(image_rep4pt1,(0.55*x_ecran,y_ecran/1.3))
            screen.blit(image_rep4pt2,(0.55*x_ecran,y_ecran/1.1))

    screen.blit(image_question,(0.06*x_ecran,y_ecran/6))
    if len(question)>38:
        screen.blit(image_question2,(0.06*x_ecran,y_ecran/4))

    positions_t_j=[position_j1,position_j2,position_j3,position_j4,position_j5,position_j6]

    #place les joueurs à la bonne position
    for i in range(nb_joueur):
        position_j=positions_t_j[i]
        placement(len(reponsesF),position_j,joueur,i+1,screen)

    #rafarîchit l'écran
    pygame.display.flip()