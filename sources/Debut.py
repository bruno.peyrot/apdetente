# Créé par icare.cigrand, le 21/03/2024 en Python 3.7
from win32api import GetSystemMetrics
import pygame

#dossier qui s'occupe de déterminer le nombre de personnes voulant jouer
def debut():
    #créer une page de la taille de l'écran
    screen = pygame. display. set_mode((0, 0), pygame. FULLSCREEN)
    #taille de l'ecran
    x_ecran = GetSystemMetrics (0)
    y_ecran = GetSystemMetrics (1)
    #définie la police du texte que l'on va chercher dan un dossier déjà existant
    police = pygame.font.Font("Fonts/OctopusGame-RpWV3.ttf",round(x_ecran/35))
    police_texte = pygame.font.Font("Fonts/OctopusGame-RpWV3.ttf",round(x_ecran/25))
    #texte à afficher dans notre début de jeu que python transforme en image
    texte_question="Bienvenue a tous !, Combiens êtes vous ?"
    image_question = police.render(texte_question,1,(255,255,255))
    #téllécharge l'image de fond
    fond=pygame.image.load("Images/Debut_fond.jpg")
    fond = pygame.transform.scale( fond , (x_ecran,y_ecran))
    valider=False
    ch=0
    #texte temporaire du nombre de joueurs
    texte=" "
    image_texte =police.render(texte,1,(255,255,0))
    #affiche le fond et le texte de la question
    screen.blit(fond,(0,0))
    screen.blit(image_texte,(x_ecran/2,y_ecran/1.5))
    screen.blit(image_question,(x_ecran/10,y_ecran/8))
    pygame.display.flip()
    while valider !=True:
        #teste si les touches sont pressés et ainsi passe le texte au nombre correspondant a la touche utilisé
        for event in pygame.event.get():
         if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_1 :
                texte="1"
                ch=1
            if event.key == pygame.K_2 :
                texte="2"
                ch=2
            if event.key == pygame.K_3 :
                texte="3"
                ch=3
            if event.key == pygame.K_4 :
                texte="4"
                ch=4
            if event.key == pygame.K_5 :
                texte="5"
                ch=5
            if event.key == pygame.K_6 :
                texte="6"
                ch=6
            if event.key == pygame.K_KP1 :
                texte="1"
                ch=1
            if event.key == pygame.K_KP2 :
                texte="2"
                ch=2
            if event.key == pygame.K_KP3 :
                texte="3"
                ch=3
            if event.key == pygame.K_KP4 :
                texte="4"
                ch=4
            if event.key == pygame.K_KP5 :
                texte="5"
                ch=5
            if event.key == pygame.K_KP6 :
                texte="6"
                ch=6
            #si la touche entré est présser alors le joueur veut commencer le jeu
            if event.key == pygame.K_RETURN and ch!=0:
                valider=True
            image_texte =police_texte.render(texte,1,(0,0,0))
            screen.blit(fond,(0,0))
            if texte=="1":
                screen.blit(image_texte,(x_ecran/2-x_ecran/90,y_ecran/1.60))
            else:
                screen.blit(image_texte,(x_ecran/2-x_ecran/60,y_ecran/1.60))
            screen.blit(image_question,(x_ecran/10,y_ecran/8))
            #rafraîchit l'écran
            pygame.display.flip()
    #renvoie le nombre de joueurs
    return ch

def ch_categorie():
    """
    Même utilisation que le programme antérieur pour le choix de la catégorie
    """
    liste_image=["Images/SVT.jpg","Images/NSI.jpg","Images/Maths.jpg","Images/SES.jpg","Images/Culture_generale.jpg","Images/Dilemmes_debats.jpg"]
    categories=["Svt","Nsi","Maths","Ses","G","D"]
    screen = pygame. display. set_mode((0, 0), pygame. FULLSCREEN)
    x_ecran = GetSystemMetrics (0)
    y_ecran = GetSystemMetrics (1)
    police = pygame.font.Font("Fonts/OctopusGame-RpWV3.ttf",round(x_ecran/70))
    police_texte = pygame.font.Font("Fonts/OctopusGame-RpWV3.ttf",round(x_ecran/25))
    texte_question="Veuiller rentrer la catégorie sélectionnée"
    image_question = police.render(texte_question,1,(255,255,255))
    fond=pygame.image.load("Images/Debut_fond_ch.jpg")
    fond = pygame.transform.scale( fond , (x_ecran,y_ecran))
    valider=False
    ch=0
    texte=" "
    image_texte =police.render(texte,1,(255,255,0))
    screen.blit(fond,(0,0))
    screen.blit(image_texte,(x_ecran/2,y_ecran/1.5))
    screen.blit(image_question,(x_ecran/3.5,y_ecran/8))
    pygame.display.flip()
    while valider !=True:
        for event in pygame.event.get():
         if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_1 :
                texte="1"
                ch=1
            if event.key == pygame.K_2 :
                texte="2"
                ch=2
            if event.key == pygame.K_3 :
                texte="3"
                ch=3
            if event.key == pygame.K_4 :
                texte="4"
                ch=4
            if event.key == pygame.K_5 :
                texte="5"
                ch=5
            if event.key == pygame.K_6 :
                texte="6"
                ch=6
            if event.key == pygame.K_KP1 :
                texte="1"
                ch=1
            if event.key == pygame.K_KP2 :
                texte="2"
                ch=2
            if event.key == pygame.K_KP3 :
                texte="3"
                ch=3
            if event.key == pygame.K_KP4 :
                texte="4"
                ch=4
            if event.key == pygame.K_KP5 :
                texte="5"
                ch=5
            if event.key == pygame.K_KP6 :
                texte="6"
                ch=6
            if event.key == pygame.K_RETURN and ch!=0:
                valider=True
            image_texte =police_texte.render(texte,1,(0,0,0))
            screen.blit(fond,(0,0))
            if texte=="1":
                screen.blit(image_texte,(x_ecran/2-x_ecran/80,y_ecran/1.60))
            else:
                screen.blit(image_texte,(x_ecran/2-x_ecran/60,y_ecran/1.60))
            screen.blit(image_question,(x_ecran/3.5,y_ecran/8))
            pygame.display.flip()
    screen.blit(pygame.transform.scale( pygame.image.load(liste_image[ch-1]) , (x_ecran,y_ecran)),(0,0))
    pygame.display.flip()
    return ch