# Créé par chiar, le 22/03/2024 en Python 3.7
from win32api import GetSystemMetrics
import pygame
import time


#dossier qui s'occupe de la transition avant les questions
def transition1(reponses):
    #enlève les réponses qui sont vide parmis le 4 propositions
    reponsesF = []
    for i in range(4):
        if reponses[i]!="":
            reponsesF.append(reponses[i])

    #taille de l'écran
    x_ecran =GetSystemMetrics (0)
    y_ecran =GetSystemMetrics (1)
    #créer une page en plein écran
    screen = pygame. display. set_mode((0, 0), pygame. FULLSCREEN)
    running = True
    #s'il n'y a que 2 réponses possibles
    if len(reponsesF)==2:
        base = pygame.image.load("Images/2R_base.jpg")
    #s'il y a 3 réponses possibles
    if len(reponsesF)==3:
        base = pygame.image.load("Images/3R_base.jpg")
    #si il y a 4 réponses possibles
    if len(reponsesF)==4:
        base = pygame.image.load("Images/4R_base.jpg")
    #transforme notre image de transition de la même taille que notre taille d'ecran
    base = pygame.transform.scale( base , (x_ecran,y_ecran))
    #mouvement de l'image qui fait notre transition
    w= x_ecran
    while w>0:
        w -= 25#3
        screen.blit(base,(w,0))
        pygame.display.flip()