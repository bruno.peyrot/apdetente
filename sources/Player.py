# Créé par chiar, le 14/03/2024 en Python 3.7
import pygame
#dossier qui s'occupe des joueurs
class Player:
    def __init__(self,x,y,image):
        #approprie son image à son joueur
        self.image = pygame.image.load(image)

    def draw(self, screen,position):
        #dessine le joueur sur l'écran en fonction de sa position
        screen.blit(self.image,position)