# Créé par pc, le 12/03/2024 en Python 3.7
import random
#ce dossier s'occupe de gérer les questions
def QG(question_passer,choix):
    #s'occupe d'ouvrir le bon fichier texte
    nb_questions=0
    categories=["G","D","Maths","Nsi","Svt","Ses","Geopo"]
    if choix =="G":
        questions= (open("Questions/Culture_general.txt","r")).readlines()
    if choix =="D":
        questions= (open("Questions/Dilemmes_débats.txt","r")).readlines()
    if choix =="Maths":
        questions= (open("Questions/Maths.txt","r")).readlines()
    if choix =="Nsi":
        questions= (open("Questions/Nsi.txt","r")).readlines()
    if choix =="Svt":
        questions= (open("Questions/Svt.txt","r")).readlines()
    if choix =="Ses":
        questions= (open("Questions/Ses.txt","r")).readlines()
    if choix =="Geopo":
        questions= (open("Questions/Geopo.txt","r")).readlines()
    for i in range (len(questions)):
        if questions[i][0]=="<":
            nb_questions+=1

    #si le dossier est vide
    if len(question_passer)==nb_questions:
        return "fin","","",""
    #si il n'y a encore aucunes questions qui sont passées
    if question_passer==[]:
        question_ch=random.randint(1,nb_questions)
    #sinon chosir une questions au hasard
    else:
        question_ch=question_passer[0]
        while question_ch in question_passer:
            question_ch=random.randint(1,nb_questions)
    question_passer.append(question_ch)
    q=0
    #s'occupe de s'adapter suivant comment on a écrit le fichier texte
    for i in range(len(questions)):
        if questions[i][0]=="<" :
            q+=1  #q pour question, je cherche où est la question dans le fichier
            if q==question_ch:
                emplacement_qch=i+1
    question_ch=(questions[emplacement_qch])[:-1]
    i=0
    reponses=["","","",""]
    #tans que la ligne n'est pas ">" c'est à dire la fin de la queston ainsi que la fin des réponses
    while questions[emplacement_qch+i+1][0]!=">":
        reponses[i]=(questions[emplacement_qch+i+1])[:-1] #je fais ça pour pas avoir les retours a la ligne
        i+=1
    nb_reponse=i
    #cherche la bonne réponse
    for i in range(nb_reponse):
        if reponses[i][len(reponses[i])-1] =="*":
            reponses[i]=(reponses[i])[:-1]
            v_reponse=i+1 # 0 = la première réponse

    #renvoie la question, les réponses, la bonne réponse ainsi que la liste des questions posées
    return question_ch,reponses,v_reponse,question_passer

