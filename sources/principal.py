# Créé par chiar, le 20/02/2024 en Python 3.7
from math import*
from time import*
from random import*
from win32api import GetSystemMetrics
import pygame
import Transition
import Question
import Affichage
import Sons
import Player
import Debut
import Fin

x_ecran = GetSystemMetrics (0)
y_ecran = GetSystemMetrics (1)

#initie les joueurs
player1 = Player.Player(5,y_ecran/1.65,"Images/piont_vert.jpg")
player2 = Player.Player(5,y_ecran/1.60,"Images/piont_bleu.jpg")
player3 = Player.Player(5,y_ecran/1.55,"Images/piont_rouge.jpg")
player4 = Player.Player(5,y_ecran/1.5,"Images/piont_jaune.jpg")
player5 = Player.Player(5,y_ecran/1.55,"Images/piont_orange.jpg")
player6 = Player.Player(5,y_ecran/1.5,"Images/piont_violet.jpg")

#ce module est le principal, il s'occupe de la bonne gestio de toutes les taches
class Game:
    #stock les informations importantes
    def __init__(self, screen):
        self.screen = screen
        self.running = True
        self.clock = pygame.time.Clock()
        self.question_passer=[]
        self.points_j1 = 0
        self.points_j2 = 0
        self.points_j3 = 0
        self.points_j4 = 0
        self.points_j5 = 0
        self.points_j6 = 0
    #gère la détection des touches par les utilisateurs
    def handling_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.running = False
        #enregistre les touches sélectionnez
        keys = pygame.key.get_pressed()

        if keys[pygame.K_SPACE]:
            #si le joueur est passer à la question suivante
            #catégories de dossier possibles
            categories=["Svt","Nsi","Maths","Ses","G","D"]
            question,reponses,v_reponse,self.question_passer=Question.QG(self.question_passer,categories[ch-1])
            #v_question = 1 donc c'est la première réponse
            #si il n' a plus de questions
            if question=="fin":
                self.running=False
            #tend que le jeu n'est pas fini cette boule tourne
            if self.running==True:
                nb_reponses =0
                #enlève les reponses vides
                for i in range(4):
                    if reponses[i]!="":
                        nb_reponses+=1
                #lance la transition avant la question
                Transition.transition1(reponses)
                #position de chaques joueurs
                position_j1=0
                position_j2=0
                position_j3=0
                position_j4=0
                position_j5=0
                position_j6=0
                #affiche la question
                Affichage.Question(question,reponses,screen,player1,player2,player3,player4,player5,player6,position_j1,position_j2,position_j3,position_j4,position_j5,position_j6,nb_joueur)
                policetimer = pygame.font.Font("Fonts/OctopusGame-RpWV3.ttf",round(x_ecran/30))
                temp=True
                #stock le timer
                t_base=round(time())
                t_ac=t_base
                while temp:
                    for event in pygame.event.get():
                        if event.type == pygame.KEYDOWN:
                            keys = pygame.key.get_pressed()
                            #si le joueur ne sais plus comment jouer avec les touches alor une image de rappel s'affiche
                            if event.key == pygame.K_TAB:
                                image_menu= pygame.transform.scale( pygame.image.load("Images/Touches.jpg"), (x_ecran,y_ecran))
                                menu=True
                                screen.blit(image_menu,(0,0))
                                pygame.display.flip()
                                while menu:
                                    for event in pygame.event.get():
                                        if event.type == pygame.KEYDOWN:
                                            keys = pygame.key.get_pressed()
                                            if keys[pygame.K_TAB] :
                                                menu=False

                            #gère tout les mouvements de position suivant si les touches sont préssées
                            if event.key == pygame.K_a:     #joueur1
                                if position_j1 !=0:
                                    position_j1 -=1
                            if event.key == pygame.K_q:
                                if position_j1 !=nb_reponses:
                                    position_j1 +=1

                            if event.key == pygame.K_c:     #joueur2
                                if position_j2 !=0:
                                    position_j2 -=1
                            if event.key == pygame.K_v:
                                if position_j2 !=nb_reponses:
                                    position_j2 +=1

                            if event.key == pygame.K_COMMA or event.key == pygame.K_QUESTION:     #joueur3
                                if position_j3 !=0:
                                    position_j3 -=1
                            if event.key == pygame.K_PERIOD or event.key == pygame.K_SEMICOLON:
                                if position_j3 !=nb_reponses:
                                    position_j3 +=1

                            if event.key == pygame.K_LEFT:     #joueur4
                                if position_j4 !=0:
                                    position_j4 -=1
                            if event.key == pygame.K_RIGHT:
                                if position_j4 !=nb_reponses:
                                    position_j4 +=1

                            if event.key == pygame.K_KP1:     #joueur5
                                if position_j5 !=0:
                                    position_j5 -=1
                            if event.key == pygame.K_KP2:
                                if position_j5 !=nb_reponses:
                                    position_j5 +=1

                            if event.key == pygame.K_KP_PLUS:     #joueur6
                                if position_j6 !=0:
                                    position_j6 -=1
                            if event.key == pygame.K_KP_MINUS:
                                if position_j6 !=nb_reponses:
                                    position_j6 +=1
                             #affiche à nouveau la question
                            Affichage.Question(question,reponses,screen,player1,player2,player3,player4,player5,player6,position_j1,position_j2,position_j3,position_j4,position_j5,position_j6,nb_joueur)
                        #arrête le temps de mouvement si la touche entrée est presser
                        if keys[pygame.K_RETURN]:
                            temp=False
                            #affiche la bonne réponse
                            Affichage.reponse(question,reponses,screen,player1,player2,player3,player4,player5,player6,position_j1,position_j2,position_j3,position_j4,position_j5,position_j6,nb_joueur,v_reponse,categories,ch)

                    #lance un timer de 15 secondes à part si le thème est les dilèmes et les débats
                    if categories[ch-1] != "D":
                        t_ac=round(time())
                        timer=str(15-(t_ac-t_base))
                        timer_image = policetimer.render(timer,1,(0,0,0))
                    #affiche la question
                    Affichage.Question(question,reponses,screen,player1,player2,player3,player4,player5,player6,position_j1,position_j2,position_j3,position_j4,position_j5,position_j6,nb_joueur)
                    #affiche le timer à part si c'est le thème débats et dilèmes
                    if categories[ch-1] != "D":
                        screen.blit(timer_image,(x_ecran*7/8,y_ecran*2.5/8))
                    pygame.display.flip()
                    #arrêtre le temps de réponse si le timer est fini
                    if t_ac-t_base >=15:
                        temp=False
                        #affiche la bonne réponse
                        Affichage.reponse(question,reponses,screen,player1,player2,player3,player4,player5,player6,position_j1,position_j2,position_j3,position_j4,position_j5,position_j6,nb_joueur,v_reponse,categories,ch)


                #compte le nombre de points des joueurs suivant combien il y en a
                if position_j1==v_reponse and nb_joueur>=1 and categories[ch-1] != "D":
                    self.points_j1 += 1
                if position_j2==v_reponse and nb_joueur>=2 and categories[ch-1] != "D":
                    self.points_j2 += 1
                if position_j3==v_reponse and nb_joueur>=3 and categories[ch-1] != "D":
                    self.points_j3 += 1
                if position_j4==v_reponse and nb_joueur>=4 and categories[ch-1] != "D":
                    self.points_j4 += 1
                if position_j5==v_reponse and nb_joueur>=5 and categories[ch-1] != "D":
                    self.points_j5 += 1
                if position_j6==v_reponse and nb_joueur>=6 and categories[ch-1
                ] != "D":
                    self.points_j6 += 1
                print("la bonne réponse étais :",reponses[v_reponse-1])


        #si la touche echape est pressé alors l jeu s'arrête
        if keys[pygame.K_ESCAPE]:
            self.running = False

    #fonction qui se sert de toutes celle d'au dessus enn boucle afin de faire la continuité du jeu
    def run(self):
        while self.running:
            self.handling_events()
            self.clock.tick(30)
        print(self.points_j1 , self.points_j2 , self.points_j3 , self.points_j4 , self.points_j5 , self.points_j6)

#programme principale qui initie le premier écran, met la musique, lance le jeu, fini le jeu et arrête me jeu
pygame.init()
pygame.mixer.init()
Sons.Musique()
screen = pygame. display. set_mode((0, 0), pygame. FULLSCREEN)
game = Game(screen)
nb_joueur = Debut.debut()
ch=Debut.ch_categorie()
game.run()
Fin.fin(game.points_j1,game.points_j2,game.points_j3,game.points_j4,game.points_j5,game.points_j6,player1,player2,player3,player4,player5,player6)
pygame.quit()