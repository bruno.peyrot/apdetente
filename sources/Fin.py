# Créé par chiar, le 26/03/2024 en Python 3.7
from math import*
from time import*
from random import*
from win32api import GetSystemMetrics
import pygame

#ce dossier s'occupe de faire la fin du jeu
def fin(points_j1 , points_j2 , points_j3 , points_j4 , points_j5 , points_j6 , player1 , player2 , player3 , player4 , player5 , player6):
    runningP = True
    while runningP:
        #Trie dans le bon son les 3 premier gagnats
        liste_j_points = [ points_j1 , points_j2 , points_j3 , points_j4 , points_j5 , points_j6 ]
        liste_gagants = []
        for i in range(3):
            final=0
            position=0
            for j in range(len(liste_j_points)):
                if liste_j_points[j]>final:
                    final=liste_j_points[j]
                    position = j

            liste_j_points[position]=0
            liste_gagants.append(position)

        #taille de l'écran
        x_ecran =GetSystemMetrics (0)
        y_ecran =GetSystemMetrics (1)
        #créer une écran
        screen = pygame. display. set_mode((0, 0), pygame. FULLSCREEN)
        running = True
        clock = pygame.time.Clock()

        #import l'image du podium
        base = pygame.image.load("Images/Podium.jpg")
        base = pygame.transform.scale( base , (x_ecran,y_ecran))
        #affiche l'image du podiem
        screen.blit(base,(0,0))


        liste_j_points = [ points_j1 , points_j2 , points_j3 , points_j4 , points_j5 , points_j6 ]

        #import notre police d'écriture
        police = pygame.font.Font("Fonts/OctopusGame-RpWV3.ttf",round(x_ecran/25))

        #liste le score des 3 premier joueurs
        score_num1 = police.render(  str(liste_j_points[liste_gagants[0]])   ,1,(255,50,50))
        score_num2 = police.render(  str(liste_j_points[liste_gagants[1]])   ,1,(255,50,50))
        score_num3 = police.render(  str(liste_j_points[liste_gagants[2]])   ,1,(255,50,50))


        #affiche le score des joueurs ainsi que leur image suivants leur pplace sur le podium
        for i in range(3):
            if liste_gagants[i] == 0:
                if i==0:
                    player1.draw(screen,(x_ecran/2.03,y_ecran/3.9))
                    screen.blit(score_num1,(x_ecran/2.05,y_ecran/2.6))
                elif i==1:
                    player1.draw(screen,(x_ecran/4.12,y_ecran/1.9))
                    screen.blit(score_num2,(x_ecran/4.13,y_ecran/1.6))
                elif i==2:
                    player1.draw(screen,(x_ecran/1.39,y_ecran/1.7))
                    screen.blit(score_num3,(x_ecran/1.4,y_ecran/1.48))



            if liste_gagants[i] == 1:
                if i==0:
                    player2.draw(screen,(x_ecran/2,y_ecran/3.9))
                    screen.blit(score_num1,(x_ecran/2.05,y_ecran/2.6))
                elif i==1:
                    player2.draw(screen,(x_ecran/4.13,y_ecran/1.9))
                    screen.blit(score_num2,(x_ecran/4.13,y_ecran/1.6))
                elif i==2:
                    player2.draw(screen,(x_ecran/1.38,y_ecran/1.7))
                    screen.blit(score_num3,(x_ecran/1.4,y_ecran/1.48))



            if liste_gagants[i] == 2:
                if i==0:
                    player3.draw(screen,(x_ecran/2,y_ecran/3.9))
                    screen.blit(score_num1,(x_ecran/2.05,y_ecran/2.6))
                elif i==1:
                    player3.draw(screen,(x_ecran/4.13,y_ecran/1.9))
                    screen.blit(score_num2,(x_ecran/4.13,y_ecran/1.6))
                elif i==2:
                    player3.draw(screen,(x_ecran/1.38,y_ecran/1.7))
                    screen.blit(score_num3,(x_ecran/1.4,y_ecran/1.48))



            if liste_gagants[i] == 3:
                if i==0:
                    player4.draw(screen,(x_ecran/2,y_ecran/3.9))
                    screen.blit(score_num1,(x_ecran/2.05,y_ecran/2.6))
                elif i==1:
                    player4.draw(screen,(x_ecran/4.13,y_ecran/1.9))
                    screen.blit(score_num2,(x_ecran/4.13,y_ecran/1.6))
                elif i==2:
                    player4.draw(screen,(x_ecran/1.38,y_ecran/1.7))
                    screen.blit(score_num3,(x_ecran/1.4,y_ecran/1.48))



            if liste_gagants[i] == 4:
                if i==0:
                    player5.draw(screen,(x_ecran/2,y_ecran/3.9))
                    screen.blit(score_num1,(x_ecran/2.05,y_ecran/2.6))
                elif i==1:
                    player5.draw(screen,(x_ecran/4.13,y_ecran/1.9))
                    screen.blit(score_num2,(x_ecran/4.13,y_ecran/1.6))
                elif i==2:
                    player5.draw(screen,(x_ecran/1.38,y_ecran/1.7))
                    screen.blit(score_num3,(x_ecran/1.4,y_ecran/1.48))



            if liste_gagants[i] == 5:
                if i==0:
                    player6.draw(screen,(x_ecran/2,y_ecran/3.9))
                    screen.blit(score_num1,(x_ecran/2.05,y_ecran/2.6))
                elif i==1:
                    player6.draw(screen,(x_ecran/4.13,y_ecran/1.9))
                    screen.blit(score_num2,(x_ecran/4.13,y_ecran/1.6))
                elif i==2:
                    player6.draw(screen,(x_ecran/1.38,y_ecran/1.7))
                    screen.blit(score_num3,(x_ecran/1.4,y_ecran/1.48))

        #rafraîchit l'écran
        pygame.display.flip()

        #si la touche espace est presser alors le jeu est fini
        keys = pygame.key.get_pressed()
        if keys[pygame.K_ESCAPE]:
            runningP = False
